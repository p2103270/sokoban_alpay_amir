package VueControleur;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;





import modele.*;

import static java.lang.Thread.sleep;


/** Cette classe a deux fonctions :
 *  (1) Vue : proposer une représentation graphique de l'application (cases graphiques, etc.)
 *  (2) Controleur : écouter les évènements clavier et déclencher le traitement adapté sur le modèle (flèches direction Pacman, etc.))
 *
 */
public class VueControleur extends JFrame implements Observer, MouseListener {
    private Jeu jeu; // référence sur une classe de modèle : permet d'accéder aux données du modèle pour le rafraichissement, permet de communiquer les actions clavier (ou souris)

    private int sizeX; // taille de la grille affichée
    private int sizeY;

    // icones affichées dans la grille
    private ImageIcon icoHero;
    private ImageIcon icoHero2;
    private ImageIcon icoVide;
    private ImageIcon icoMur;
    private ImageIcon icoBloc;

    private ImageIcon icoTP;
    private ImageIcon icoBloc_SO;
    private ImageIcon icoCheckPoint;

    private ImageIcon icoBlocD;


    private int etat; // Different etat pour gerer les differentes JFrames a afficher

    private JFrame frame_demarrage;//frame pour le demarrage
    private JFrame frame_fin;//frame pour la fin
    private JFrame frame_chargement;//frame pour le chargement

    private JFrame frame_perdu;//frame si on perd

    private JLabel[][] tabJLabel; // cases graphique (au moment du rafraichissement, chaque case va être associée à une icône, suivant ce qui est présent dans le modèle)

    private  boolean perdu = false;  //permet de savoir si nous avons perdu

    private boolean nether = false;

    private Clip clip;  //musique a jouer lors des differentes frames
    private Clip musique_fond;  //musique de fonds a jouer lorsqu'on joue

    public VueControleur(Jeu _jeu) {
        demarrage_panel();
        sizeX = _jeu.SIZE_X;
        sizeY = _jeu.SIZE_Y;
        jeu = _jeu;
        chargerLesIcones();
        placerLesComposantsGraphiques();
        ajouterEcouteurClavier();
        icoHero = chargerIcone("Images/Front_steve.png");
        icoHero2 = chargerIcone("Images/front_alex.png");
        jeu.addObserver(this);
        mettreAJourAffichage();
    }


    //procédure qui initialise le Jframe de démarrage
    private void demarrage_panel(){

        lire_son("Minecraft_accueil2.wav");
        etat = 0;                                        //on initialise l'etat de la fenêtre
        frame_demarrage  = new JFrame("Demarrage"); //declaration du frame pour le demarrage
        //frame_demarrage.setSize(20*sizeX, 40*sizeY);
        frame_demarrage.setVisible(true);               //on déclare la fenêtre visible
        JPanel panel = new JPanel();                    //on déclare le Jpanel qui sera rattaché au JFrame
        panel.addMouseListener(this);                 //Permet de recuperer l'action de la souris
        ImageIcon icon = new ImageIcon("Images/accueil_4.gif"); //ajout de l'image au panel
        panel.add(new JLabel(icon));

        //Permet de fermer le programme si on quitte la fenêtre
        frame_demarrage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //On ajoute le panel a la Jframe
        frame_demarrage.add(panel);

        //Permet de dimensionner l'image correctement
        frame_demarrage.pack();
    }

    //fonction pour jouer la musique
    private void lire_son(String t) {
        // Play Audio  File
        try {
            String filepath = "Sounds/"+t;
            AudioInputStream aui = AudioSystem.getAudioInputStream(new File(filepath).getAbsoluteFile());
            try {
                clip = AudioSystem.getClip();
                clip.open(aui);
                clip.start();   //start Clip
                /*clip.stop();    //Stop Clip
                clip.loop(10); // 10 time loop clip
                clip.close();*/
            } catch (IOException | LineUnavailableException e) {
            }


        } catch (IOException | UnsupportedAudioFileException e) {
        }
    }

    //fonction pour arreter le musique
    private void fermer_song(Clip clip){
        clip.stop();
        clip.close();
    }

    private void lire_son_fond() {
        // Play Audio  File
        try {
            String filepath = "Sounds/game.wav";
            AudioInputStream aui = AudioSystem.getAudioInputStream(new File(filepath).getAbsoluteFile());
            try {
                musique_fond = AudioSystem.getClip();
                musique_fond.open(aui);
                musique_fond.start();   //start Clip
                //clip.stop();    //Stop Clip
                musique_fond.loop(2); // 10 time loop clip
                //clip.close();*/
            } catch (IOException | LineUnavailableException e) {
            }


        } catch (IOException | UnsupportedAudioFileException e) {
        }
    }

    private void fermer_song_fond(Clip clip){
        musique_fond.stop();
        musique_fond.close();
    }



    //L'ecoute du clavier, on gère trois grands cas : si nous sommes en mode solo ou duo, puis si nous sommes dans le nether et enfin si nous sommes en mode solo mais avec
    // un second hero present
    private void ajouterEcouteurClavier() {
        addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme, il s'agit d'un objet qui correspond au controleur dans MVC
            @Override
            public void keyPressed(KeyEvent e) {
                if(!jeu.getMode_2_joueurs()) {
                    if(!jeu.get_A_travers_autre_carte())
                    {
                        switch (e.getKeyCode()) {  // on regarde quelle touche a été pressée
                            case KeyEvent.VK_LEFT:
                                if(jeu.get_hero_controlable()){
                                    icoHero = chargerIcone("Images/Left_steve.png");
                                    lire_son("son_wood_rac.wav");
                                }else{
                                    icoHero2 = chargerIcone("Images/left_alex.png");
                                    lire_son("son_wood_rac.wav");
                                }
                                jeu.deplacerHeros(Direction.gauche);
                                break;

                            case KeyEvent.VK_RIGHT:
                                if(jeu.get_hero_controlable()){
                                    icoHero = chargerIcone("Images/Right_steve.png");
                                    lire_son("son_wood_rac.wav");
                                }else{
                                    icoHero2 = chargerIcone("Images/alex_right.png");
                                    lire_son("son_wood_rac.wav");
                                }
                                jeu.deplacerHeros(Direction.droite);
                                break;

                            case KeyEvent.VK_DOWN:
                                if(jeu.get_hero_controlable()){
                                    icoHero = chargerIcone("Images/Front_steve.png");
                                    lire_son("son_wood_rac.wav");
                                }else{
                                    icoHero2 = chargerIcone("Images/front_alex.png");
                                    lire_son("son_wood_rac.wav");
                                }
                                jeu.deplacerHeros(Direction.bas);
                                break;

                            case KeyEvent.VK_UP:
                                if(jeu.get_hero_controlable()){
                                    icoHero = chargerIcone("Images/Back_steve.png");
                                    lire_son("son_wood_rac.wav");
                                }else{
                                    icoHero2 = chargerIcone("Images/back_alex.png");
                                    lire_son("son_wood_rac.wav");
                                }
                                jeu.deplacerHeros(Direction.haut);
                                break;

                            case KeyEvent.VK_SPACE:
                                if(jeu.getsecondheros()){
                                    jeu.set_alterner_joueur();
                                    break;
                                }
                                break;

                            case KeyEvent.VK_R:
                                jeu.retry();
                                break;

                            case KeyEvent.VK_ESCAPE:
                                System.exit(0);
                                break;
                        }
                    }else {
                        switch (e.getKeyCode()) {  // on regarde quelle touche a été pressée
                            case KeyEvent.VK_LEFT:
                                icoHero = chargerIcone("Images/nether_right.png");
                                lire_son("son_wood_rac.wav");
                                jeu.deplacerHeros(Direction.gauche);
                                break;

                            case KeyEvent.VK_RIGHT:
                                icoHero = chargerIcone("Images/nether_left.png");
                                lire_son("son_wood_rac.wav");
                                jeu.deplacerHeros(Direction.droite);
                                break;

                            case KeyEvent.VK_DOWN:
                                icoHero = chargerIcone("Images/front_block.png");
                                lire_son("son_wood_rac.wav");
                                jeu.deplacerHeros(Direction.bas);
                                break;

                            case KeyEvent.VK_UP:
                                icoHero = chargerIcone("Images/nether_back.png");
                                lire_son("son_wood_rac.wav");
                                jeu.deplacerHeros(Direction.haut);
                                break;

                            case KeyEvent.VK_SPACE:
                                if(jeu.getsecondheros()){
                                    jeu.set_alterner_joueur();
                                    break;
                                }
                                break;
                            case KeyEvent.VK_R:
                                jeu.retry();
                                break;

                            case KeyEvent.VK_ESCAPE:
                                System.exit(0);
                                break;

                        }
                    }
                }else
                {
                    switch (e.getKeyCode()) {  // on regarde quelle touche a été pressée
                        case KeyEvent.VK_LEFT:
                            icoHero2 = chargerIcone("Images/left_alex.png");
                            lire_son("son_wood_rac.wav");
                            jeu.deplacerHeros2Coop(Direction.gauche);
                            break;

                        case KeyEvent.VK_RIGHT:
                            icoHero2 = chargerIcone("Images/alex_right.png");
                            lire_son("son_wood_rac.wav");
                            jeu.deplacerHeros2Coop(Direction.droite);
                            break;

                        case KeyEvent.VK_DOWN:
                            icoHero2 = chargerIcone("Images/front_alex.png");
                            lire_son("son_wood_rac.wav");
                            jeu.deplacerHeros2Coop(Direction.bas);
                            break;

                        case KeyEvent.VK_UP:
                            icoHero2 = chargerIcone("Images/back_alex.png");
                            lire_son("son_wood_rac.wav");
                            jeu.deplacerHeros2Coop(Direction.haut);
                            break;

                        case KeyEvent.VK_Q:
                            icoHero = chargerIcone("Images/Left_steve.png");
                            lire_son("son_wood_rac.wav");
                            jeu.deplacerHeros(Direction.gauche);
                            break;

                        case KeyEvent.VK_D:
                            icoHero = chargerIcone("Images/Right_steve.png");
                            lire_son("son_wood_rac.wav");
                            jeu.deplacerHeros(Direction.droite);
                            break;

                        case KeyEvent.VK_S:
                            icoHero = chargerIcone("Images/Front_steve.png");
                            lire_son("son_wood_rac.wav");
                            jeu.deplacerHeros(Direction.bas);
                            break;

                        case KeyEvent.VK_Z:
                            icoHero = chargerIcone("Images/Back_steve.png");
                            lire_son("son_wood_rac.wav");
                            jeu.deplacerHeros(Direction.haut);
                            break;

                        case KeyEvent.VK_SPACE:
                            break;
                        case KeyEvent.VK_R:
                            jeu.retry();
                            break;
                        case KeyEvent.VK_ESCAPE:
                            System.exit(0);
                            break;

                    }
                }
            }
        });
    }

    //Permet de charger les differentes images
    private void chargerLesIcones() {
        if(jeu.get_A_travers_autre_carte()==false) {
            nether = false;
            icoVide = chargerIcone("Images/Wood.png");
            icoMur = chargerIcone("Images/mur.jpg");
            icoBloc = chargerIcone("Images/Gold.png");
            icoCheckPoint = chargerIcone("Images/piece.png");
            icoTP = chargerIcone("Images/enderpearl.png");
            icoBloc_SO = chargerIcone("Images/redstone.png");
            icoBlocD = chargerIcone("Images/Woods.png");
        }
        else
        {
            //les images doivent changer en fonction de de si nous passons le TP_map
            if(nether==false){
                icoHero = chargerIcone("Images/front_block.png");
                icoHero2 = chargerIcone("Images/front_alex.png");
                nether = true;
            }
            icoVide = chargerIcone("Images/nether_block.png");
            icoMur = chargerIcone("Images/Nether_Bricks.png");
            icoBloc = chargerIcone("Images/Gold.png");
            icoCheckPoint = chargerIcone("Images/piece.png");
            icoTP = chargerIcone("Images/enderpearl.png");
            icoBloc_SO = chargerIcone("Images/redstone.png");
            icoBlocD = chargerIcone("Images/Woods.png");
        }
    }


    //permet de charger une image depuis un chemin
    private ImageIcon chargerIcone(String urlIcone) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(urlIcone));
        } catch (IOException ex) {
            Logger.getLogger(VueControleur.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return new ImageIcon(image);
    }


    //Projet ecran de chargement, pour passer au niveau superieur, recommencer ou quitter
    private void ecran_chargement(){

        lire_son("succes_song_rac.wav");
        frame_chargement = new JFrame("charge");
        frame_chargement.setVisible(true);
        setVisible(false);
        JPanel panel_charge = new JPanel();
        ImageIcon iconee = new ImageIcon("Images/chargement2.gif");
        panel_charge.add(new JLabel(iconee));
        frame_chargement.add(panel_charge);
        panel_charge.addMouseListener(this);
        frame_chargement.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame_chargement.add(panel_charge);
        frame_chargement.pack();
    }




    private void placerLesComposantsGraphiques() {
        getContentPane().removeAll();
        setTitle("Sokoban");
        setSize(40*sizeX, 50*sizeY);        //new version 40*SizeX 50*SizeY   //old version 20*SizeX 40*Sizey
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        //Création d'un menu a la frame du Jeu
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Menu");

        //Création du bouton retry plus definition de son action
        JButton btn_retry = new JButton("retry");
        btn_retry.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jeu.retry();
            }
        });

        //Création du bouton exit plus definition de son action
        JButton btn_exit = new JButton("exit");
        btn_exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        });

        //ajout du bouton dans le menu, puis du menu dans la barre de menu
        menu.add(btn_retry);
        menu.add(btn_exit);
        menuBar.add(menu);
        setJMenuBar(menuBar);

        JComponent grilleJLabels = new JPanel(new GridLayout(sizeY, sizeX)); // grilleJLabels va contenir les cases graphiques et les positionner sous la forme d'une grille

        tabJLabel = new JLabel[sizeX][sizeY];
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                JLabel jlab = new JLabel();
                tabJLabel[x][y] = jlab; // on conserve les cases graphiques dans tabJLabel pour avoir un accès pratique à celles-ci (voir mettreAJourAffichage() )
                grilleJLabels.add(jlab);
            }
        }
        add(grilleJLabels);
    }

    
    /**
     * Il y a une grille du côté du modèle ( jeu.getGrille() ) et une grille du côté de la vue (tabJLabel)
     */
    //Permet de mettre a jour l'affichage de la carte
    private void mettreAJourAffichage() {
        int nbheros= 0;
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {

                Case c = jeu.getGrille()[x][y];

                if (c != null) {

                    Entite e = c.getEntite();

                    //On traitre les differentes classes, puis en leurs fonctions on affiche les différentes images
                    if (e != null) {
                        if (c.getEntite() instanceof Heros) {
                            if(nbheros==0){
                                tabJLabel[x][y].setIcon(icoHero);
                                tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                                tabJLabel[x][y].setOpaque(true);
                                nbheros++;
                            }
                            else if (nbheros==1)
                            {
                                tabJLabel[x][y].setIcon(icoHero2);
                                tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                                tabJLabel[x][y].setOpaque(true);
                            }

                        }
                        else if (c.getEntite() instanceof Bloc) {
                            tabJLabel[x][y].setIcon(icoBloc);
                            tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                            tabJLabel[x][y].setOpaque(true);
                        }
                        else if (c.getEntite() instanceof Bloc_sans_objt) {
                            tabJLabel[x][y].setIcon(icoBloc_SO);
                            tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                            tabJLabel[x][y].setOpaque(true);
                        }
                        else if (c.getEntite() instanceof Bloc_lourd) {
                            tabJLabel[x][y].setIcon(icoBlocD);
                            tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                            tabJLabel[x][y].setOpaque(true);
                        }
                    } else {
                        if (jeu.getGrille()[x][y] instanceof Mur) {
                            tabJLabel[x][y].setIcon(icoMur);
                            tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                            tabJLabel[x][y].setOpaque(true);
                        } else if (jeu.getGrille()[x][y] instanceof Vide) {
                            tabJLabel[x][y].setIcon(icoVide);
                            tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                            tabJLabel[x][y].setOpaque(true);
                        } else if (jeu.getGrille()[x][y] instanceof CheckPoint) {
                            tabJLabel[x][y].setIcon(icoCheckPoint);
                            tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                            tabJLabel[x][y].setOpaque(true);
                        } else if (jeu.getGrille()[x][y] instanceof TP) {
                            tabJLabel[x][y].setIcon(icoTP);
                            tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                            tabJLabel[x][y].setOpaque(true);
                        } else if (jeu.getGrille()[x][y] instanceof TP_map) {
                            ImageIcon portal = new ImageIcon("Images/nether.gif");
                            tabJLabel[x][y].setIcon(portal);
                            tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                            tabJLabel[x][y].setOpaque(true);
                        } else if (jeu.getGrille()[x][y] instanceof Malus_Niveau) {
                            ImageIcon lava = new ImageIcon("Images/lava_g.gif");
                            tabJLabel[x][y].setIcon(lava);
                            tabJLabel[x][y].setBackground(new Color(0, 0, 0, 0));
                            tabJLabel[x][y].setOpaque(true);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {

        //Si nous avons perdu, on charge la frame_perdu
        if(jeu.jeu_perdu()){
            fermer_song_fond(musique_fond);
            setVisible(false);
            frame_perdu = new JFrame("perdu");
            frame_perdu.setVisible(true);
            JPanel panel_fin = new JPanel();
            panel_fin.addMouseListener(this);
            ImageIcon icon = new ImageIcon("Images/lava_death2.gif"); //ajout de l'image au panel
            panel_fin.add(new JLabel(icon));
            perdu = true;
            frame_perdu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame_perdu.add(panel_fin);
            frame_perdu.pack();
            fermer_song(clip);
            lire_son("nether_accueil2.wav");
        }


        if(jeu.getFin()){
            fermer_song_fond(musique_fond);

            //Condition lorsqu'on finie tous les niveaux
            if(jeu.get_niveau_actuel()== jeu.get_total_niveau()){
                lire_son("ending_rac.wav");
                etat++;                             //On change l'état de la fenetre
                setVisible(false);                  //On cache la fentre de jeu

                //On crée et parametre la JFrame de Fin
                frame_fin  = new JFrame("Fin");
                frame_fin.setSize(300*2, 250*2);
                frame_fin.setVisible(true);

                JPanel panel_fin = new JPanel();
                ImageIcon icon = new ImageIcon("Images/end_3.gif");
                panel_fin.add(new JLabel(icon));

                frame_fin.add(panel_fin);
                panel_fin.addMouseListener(this);   //Permet de recuperer l'action de la souris

                frame_fin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame_fin.add(panel_fin);
                frame_fin.pack();
            }
            //Dans le cas on a fini un niveau mais pas l'entièreté du jeu
            if(jeu.get_niveau_actuel() < jeu.get_total_niveau()){
                jeu.setSecond_heros_present();
                ecran_chargement();

            }
            mettreAJourAffichage();
        }else{
            if(jeu.get_A_travers_autre_carte()){
                chargerLesIcones();
            }
            else{
                //Dans le cas ou nous sommes tjrs dans le jeu, on met a jours l'affichage
                if(jeu.getTp_utiliser())
                {
                    lire_son("ender_pearl.wav");
                }
                if(jeu.Tp_utiliser_nether()){
                    lire_son("nether_portal_sound.wav");
                }
                chargerLesIcones();
            }
            if(jeu.Tp_utiliser_nether()){
                lire_son("nether_portal_sound.wav");
                jeu.Set_Tp_utiliser_nether();
            }
            mettreAJourAffichage();
        }

        /*
        // récupérer le processus graphique pour rafraichir
        // (normalement, à l'inverse, a l'appel du modèle depuis le contrôleur, utiliser un autre processus, voir classe Executor)
        SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        mettreAJourAffichage();
                    }
                }); 
        */
    }


    //Différentes fonctions qui permettent de traiter les differents evenements de la souris
    @Override
    public void mouseClicked(MouseEvent e) {
        int x = e.getX(); // Récupération de la position x du clic de souris
        int y = e.getY(); // Récupération de la position y du clic de souris
        //System.out.println("Clic de souris à la position : (" + x + ", " + y + ")");

        //En fonction de l'état de la fenetre la zone dans laquelle nous cliquons aura differentes actions
        //ecran de demarrage

        if(etat==0){
            if(x>247 && x <483 && y>237 && y<314){   //permet de lancer le jeu, mode solo
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                frame_demarrage.setVisible(false);  //On ferme la fenetre de demarrage
                frame_demarrage.dispose();
                setVisible(true);
                etat++;
                lire_son_fond();

            }
            if(x>245 && x<490 && y>429 && y<513){ //Permet de quitter le jeu
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                System.exit(0);
            }

            if(x>248 && x <482 && y>333 && y<407){ //Permet de lancer le mode duo
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                jeu.setMode_2_joueurs();
                jeu.activer2joueurs();
                frame_demarrage.setVisible(false);  //On ferme la fenetre de demarrage
                frame_demarrage.dispose();
                setVisible(true);
                lire_son_fond();
                etat++;
            }
            mettreAJourAffichage();

        }
        //ecran de jeu
        if(etat == 1 && !perdu && jeu.getFin()){  //Aucune action à faire lorsque nous jouons

            if(x>41 && x<269 && y>264 && y<342){ //recommencer
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                frame_chargement.setVisible(false);;
                frame_chargement.dispose();
                jeu.retry();
                setVisible(true);
                lire_son_fond();
            }
            if(x>436 && x<670 && y>265 && y<344){//niveau suivant
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                frame_chargement.setVisible(false);
                //frame_chargement.dispose();
                jeu.niveau_dessus();                                //On augment le niveau et on met a jour l'affichage
                setVisible(true);
                lire_son_fond();
            }
            if(x>247 && x<470 && y>415 && y<493){//quitter
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                System.exit(0);
            }
            mettreAJourAffichage();



        }else if(etat == 1 && perdu){//recommencer
            if(x>283 && x<488 && y>247 && y<353){
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                frame_perdu.setVisible(false);
                frame_perdu.dispose();
                jeu.retry();
                setVisible(true);
                perdu = false;
                lire_son_fond();
            }
            if(x>281 && x<490 && y>394 && y<498){//quitter
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                System.exit(0);
            }


        }
        //ecran de fin
        if(etat == 2){
            if(x>254 && x<476 && y>168 && y<251){     //Permet de relancer le jeu, en recommancant le niveau 1
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                jeu.reset_niveau();
                etat--;
                frame_fin.dispose();
                jeu.retry();
                setVisible(true);
                lire_son_fond();
                setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
            if(x>254 && x<477 && y>432 && y<515){   //Permet de quitter
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                System.exit(0);
                frame_fin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
            if(x>255 && x<473 && y>291 && y<370 ){//permet de revenir a l'accueil
                fermer_song(clip);
                lire_son("bouton_clique.wav");
                frame_fin.dispose();
                frame_fin.setVisible(false);
                Jeu accueil_retour = new Jeu();
                VueControleur nouvelle_temp = new VueControleur(accueil_retour);
                setVisible(false);
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
