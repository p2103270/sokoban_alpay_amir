package modele;

import java.awt.*;

//Case qui provoque la téléportation du personnage
public class TP extends Case{

    private Point p; //Coordonnées de la case de TP
    public Point a_tp; //Coordonnées de la case sur laquelle il faudra se téléporter

    private int id; //id de la case TP
    private boolean parcourus=false;

    public TP(Jeu _jeu){super(_jeu);}

    public boolean peutEtreParcouru(){return true;}

    public void set_id_tp(int x){
        id = x;
    }

    public void set_point_tp(int x, int y){
        p = new Point(x,y);
    }//Set les coordonnées de la case TP initiale

    public int get_point_p_x(){return p.x;}
    public int get_point_p_y(){return p.y;}

    public void set_tp_a_tp(int x,int y){
        a_tp = new Point(x,y);
    }   //Set les coordonnées sur lesquelle il faut TP
    public Point get_tp_a_tp(){
        return a_tp;
    } //Get les coordonnées sur lesquelle il faut TP

}
