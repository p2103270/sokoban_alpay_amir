package modele;

//Bloc qu'on peut pousser sans aucune utilité ne sert pas à gagner
public class Bloc_sans_objt extends Entite {

    public Bloc_sans_objt(Jeu _jeu, Case c) {
        super(_jeu, c);
    }

    public boolean pousser(Direction d) {
        return jeu.deplacerEntite(this, d);
    }
}
