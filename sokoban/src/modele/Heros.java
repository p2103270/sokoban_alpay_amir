/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/**
 * Héros du jeu
 */
public class Heros extends Entite
{
    private  int id;
    private boolean controlable = true;
    public void setControlable_true(){controlable = true;}
    public void setControlable_false(){controlable = false;}

    public int getId(){return id;}

    public void setId(int i) {
        id = i;
    }

    public boolean getControlable(){return controlable;}

    public Heros(Jeu _jeu, Case c) {
        super(_jeu, c);
    }


}
