package modele;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

//Classe qui permet générer les différents niveaux grâce à des fichiers textes
public class Outil {
    public Outil() {

    }

    private int tailleX;
    private int tailleY;
    private int nbCheckPoint;

    private int nbBlockSansObj;

    private int nbBlock;

    private int nbBlockDestructible;
    private  int terrain[][];

    public void lire_un_fichier(String lien_du_fichier) {
        try {
            File fichier = new File(lien_du_fichier);
            Scanner scanner = new Scanner(fichier);

            // Lecture de la taille de X, la taille de Y et le nombre de checkpoints
            this.tailleX = scanner.nextInt();
            //System.out.println(tailleX);
            this.tailleY = scanner.nextInt();
            //System.out.println(tailleY);
            this.nbCheckPoint = scanner.nextInt();

            this.nbBlockSansObj = scanner.nextInt();

            this.nbBlockDestructible = scanner.nextInt();

            this.nbBlock=nbCheckPoint;

            // Lecture du terrain
            this.terrain = new int[tailleX][tailleY];
            for (int i = 0; i <tailleY ; i++) {
                for (int j = 0; j < tailleX; j++) {
                    terrain[j][i] = scanner.nextInt();

                }
            }
        } catch (IOException e) {
            System.err.println("Erreur de lecture du fichier : " + e.getMessage());

        }
    }

    public int getTailleX() {
        return tailleX;
    }

    public int getTailleY() {
        return tailleY;
    }

    public int[][] getTerrain() {
        return terrain;
    }

    public int getNbBlockSansObj(){return nbBlockSansObj;}
    public int getNbBlock(){return  nbBlock;}
    public int getNbCheckPoint() {
        return nbCheckPoint;
    }

    public int getNbBlockDestructible(){return  nbBlockDestructible;}

    public String toString() {
        StringBuilder plateau = new StringBuilder();
        for (int x = 0; x < this.tailleX; x++) {
            for (int y = 0; y < this.tailleY; y++) {
                plateau.append(terrain[x][y]).append(" ");
            }
            plateau.append("\n");
        }
        return "Plateau{" +
                "longueur=" + this.tailleX +
                ", largeur=" + this.tailleY +
                ", etatIdPlateau= \n" + plateau +
                '}';

    }

}