package modele;

public class Bloc_lourd extends Entite {

    private boolean est_detruit = false;

    public void setEst_pousse() {est_detruit = true; }
    public void setPas_pousse() {est_detruit = false; }

    public Bloc_lourd(Jeu _jeu,Case c) {
        super(_jeu,c);
    }

    public boolean pousser(Direction d) {
        if(est_detruit){
            return jeu.deplacerEntite(this, d);
        }else
        {
            return false;
        }

    }

}
