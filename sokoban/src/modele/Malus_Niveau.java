package modele;

//Case qui provoque un changement de niveau (regression)
public class Malus_Niveau extends Case {

    public Malus_Niveau(Jeu _jeu){super(_jeu);}
    @Override
    public boolean peutEtreParcouru() {
        return true;
    }

}
