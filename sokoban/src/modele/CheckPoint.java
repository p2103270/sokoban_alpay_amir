package modele;

//Case sur laquelle on doit pousser les blocs pour qu'on puisse passer au niveau suivant/finir jeux
public class CheckPoint extends Case{
    private boolean parcourus=false;
    public CheckPoint(Jeu _jeu){super(_jeu);}

    @Override
    public boolean peutEtreParcouru() {
        return true;
    }

    public void setParcourus(boolean v)
    {
        parcourus=v;
    }
    public boolean getParcourus()
    {
        return parcourus;
    }

}
