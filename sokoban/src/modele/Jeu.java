/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;


import java.awt.Point;
import java.util.HashMap;
import java.util.Observable;


public class Jeu extends Observable {

    public  int SIZE_X = 20;
    public  int SIZE_Y = 20;
    private Outil outil;



    private boolean mode_2_joueurs;

    private Heros heros; //joueur principale
    private Heros heros2;// joueur secondaire ou joueur controlable par le J1

    private Boolean second_heros_present = false;

    private Bloc[] b;
    private Bloc_sans_objt[] b_s_o;     //Tableau de bloc qui ne permettent pas de remplir le niveau
    private int numero_niveau;          //Le numero du niveau actuel
    private int total_niveau = 4;       //Le total de niveau a saisir manuellement

    private int compteurCheckPoint;     //Le nombre de checkpoint présent dans un niveau

    private CheckPoint[] c;             //Tableau de checkpoint

    private Malus_Niveau MN;            //La case Malus

    private TP[] t;                     //Tableau de TP (tp au sein de la même carte)

    private TP_map t_map;               //La case TP (a travers une autre carte)

    private boolean A_travers_autre_carte;  //Permet de savoir si on est dans une autre carte
    private  boolean Tp_utiliser = false;   //Permet de savoir si nous avons utilise la TP (pour pouvoir jouer le son)
    private boolean Tp_utiliser_nether= false; //Permet de savoir si nous avons utilise la TP (pour pouvoir jouer le son)

    private boolean perdu = false; //Permet de savoir si nous avons perdu

    private Bloc_lourd[] bd;    //Tableau de bloc lourd

    private int i = 0;      //Compteur pour connaitre le nombre d'essai avant de pouvoir pousser le bloc lourd

    private HashMap<Case, Point> map = new  HashMap<Case, Point>(); // permet de récupérer la position d'une case à partir de sa référence
    private Case[][] grilleEntites = new Case[SIZE_X][SIZE_Y]; // permet de récupérer une case à partir de ses coordonnées

    private Case[][] grilleEntites_TP = new Case[SIZE_X][SIZE_Y];   //Nouvelle map pour le TP de terrain

    public Jeu() {
        initialisationNiveau(1);
        numero_niveau = 1;
    }

    public boolean getTp_utiliser(){
        return  Tp_utiliser;
    }   //Permet de savoir si la TP_map a été utilisé
    public boolean getsecondheros(){return  second_heros_present;} //Permet de savoir si le terrain possède un second Hero
    public boolean getMode_2_joueurs(){return mode_2_joueurs;}  //Permet de savoir si le jeu se lance en mode Duo
    public boolean get_hero_controlable(){return heros.getControlable();} //Permet de savoir si l'Hero est controlable
    public int get_niveau_actuel(){return numero_niveau;} //Retourne le niveau actuel
    public boolean get_A_travers_autre_carte(){return A_travers_autre_carte;} // Permet de savoir si nous sommes dans l'autre carte
    public int get_total_niveau(){return total_niveau;} // Retourne le total de niveau
    public Case[][] getGrille() {
        return grilleEntites;
    } // Retourne la grille
    public boolean getFin(){return (c.length==compteurCheckPoint);} // Permet de savoir si le niveau est finie
    public boolean Tp_utiliser_nether(){return Tp_utiliser_nether;}  //Permet de savoir si la TP_map a été utilisé

    public  void Set_Tp_utiliser_nether(){
        Tp_utiliser_nether = false;
    }

    // Parametre a geré pour le mode 2 joueurs
    public void setMode_2_joueurs(){
        mode_2_joueurs = true;
        initialisationNiveau(numero_niveau);
    }
    public void setSecond_heros_present(){second_heros_present = false;}

    // Permet de gérer l'autorisation de déplacement (mode solo)
    public void set_alterner_joueur(){
        if(second_heros_present) {
            if (heros.getControlable()) {
                heros.setControlable_false();
                heros2.setControlable_true();
            } else {
                heros.setControlable_true();
                heros2.setControlable_false();
            }
        }
    }

    //procédure pour recommencer un niveau
    public void retry(){
        initialisationNiveau(numero_niveau);
        perdu = false;
        reset_bloc_lourd();
        A_travers_autre_carte = false;
        setChanged();
        notifyObservers();
    }


    public void reset_bloc_lourd(){
        i = 0;
    } // Permet de reset la variable des blocs lourds

    // Active le mode 2 joueurs
    public void activer2joueurs()
    {
        heros.setControlable_true();
        heros2.setControlable_true();
    }

    //procédure pour passez au niveau au dessus
    public void niveau_dessus(){
        if(numero_niveau<total_niveau)
        {
            numero_niveau++;
            initialisationNiveau(numero_niveau);
            reset_bloc_lourd();
            perdu = false;
        }
    }

    //procédure pour mettre fin au jeu
    public void passer_malus(){
        perdu = true;
        A_travers_autre_carte = false;
    }

    public boolean jeu_perdu(){
        return perdu;
    } //Permet de savoir si le jeu est perdu
    public void reset_niveau(){numero_niveau = 1;} // Permet de reset les niveaux

    //Permet de pouvoir deplacer le second Hero (uniquement mode coop)
    public void deplacerHeros2Coop(Direction d){
        heros2.avancerDirectionChoisie(d);
        setChanged();
        notifyObservers();
    }

    // Permet de deplacer l'hero et gère le cas s'il y a un autre hero (mode solo)
    public void deplacerHeros(Direction d) {
            if (heros.getControlable()) {
                heros.avancerDirectionChoisie(d);
                setChanged();
                notifyObservers();
            } else {
                heros2.avancerDirectionChoisie(d);
                setChanged();
                notifyObservers();
            }
    }

    // Permet de construire le terrain
    private void construire_terrain(int n,Outil outil){
        //n = 0 terrain normal, n == 1 tp_map, n == 2 retour_map //n==3 malus retour

        SIZE_X= outil.getTailleX();             //On définit les dimensions du Terrain
        SIZE_Y= outil.getTailleY();

        //On initialise chaque élément qui en a besoin
        t_map = new TP_map(this);
        b=new Bloc[outil.getNbBlock()];
        b_s_o=new Bloc_sans_objt[outil.getNbBlockSansObj()];
        bd = new Bloc_lourd[outil.getNbBlockDestructible()];

        compteurCheckPoint=0;
        c=new CheckPoint[outil.getNbCheckPoint()];
        for(int i=0;i<outil.getNbCheckPoint();i++){
            c[i]=new CheckPoint(this);
        }

        t = new TP[4];
        for(int i = 0; i<4 ; i++){
            t[i]=new TP(this);
        }
        //On initialise chaque compteur
        int nbBlock=0;
        int nbCheck=0;
        int nbBlockSO = 0;
        int nbTP=0;
        int nbTP_map=0;
        int nbBD = 0;

        if(n == 1)
        {
            grilleEntites_TP= new Case[SIZE_X][SIZE_Y];
        }else
        {
            grilleEntites= new Case[SIZE_X][SIZE_Y];
        }

        int [][] terrain= outil.getTerrain();

        for(int x=0;x<SIZE_X;x++)
        {
            for(int y=0;y<SIZE_Y;y++)
            {
                //System.out.println(terrain[x][y]);
                if(terrain[x][y]==0)//cas du vide
                {
                    addCase(new Vide(this), x, y);
                } else if(terrain[x][y]==1) {//cas du mur
                    addCase(new Mur(this), x, y);
                } else if (terrain[x][y]==2) {//cas du hero

                    addCase(new Vide(this), x, y);
                    if(n == 0 || n == 3){
                        heros=new Heros(this, grilleEntites[x][y]);
                    }else if(n == 1)
                    {
                        heros=new Heros(this, grilleEntites_TP[x][y]);
                    }
                    heros.setId(0);
                } else if (terrain[x][y]==3) {//cas du checkpoint
                    addCase(c[nbCheck],x,y);
                    nbCheck++;
                } else if (terrain[x][y]==4) {//cas du block
                    if(n == 1)
                    {
                        addCase(new Vide(this), x, y);
                        b[nbBlock] = new Bloc(this, grilleEntites_TP[x][y]);
                        nbBlock++;
                    }else {
                        addCase(new Vide(this), x, y);
                        b[nbBlock] = new Bloc(this, grilleEntites[x][y]);
                        nbBlock++;
                    }
                }
                else if (terrain[x][y]==5){//cas du tp
                    addCase(t[nbTP],x,y);
                    t[nbTP].set_point_tp(x,y);
                    t[nbTP].set_id_tp(nbTP);
                    nbTP++;
                }
                else if (terrain[x][y]==6){//cas du tp
                    if(n == 1)
                    {
                        addCase(t_map,x,y);
                        t_map.set_id_tp(nbTP_map);
                        nbTP_map++;
                        t_map.set_changez_carte();
                    }else{
                        addCase(t_map,x,y);
                        t_map.set_id_tp(nbTP_map);
                        nbTP_map++;
                    }
                }
                else if (terrain[x][y]==7) {//cas du block
                    addCase(new Vide(this), x, y);
                    b_s_o[nbBlockSO]=new Bloc_sans_objt(this,grilleEntites[x][y]);
                    nbBlockSO++;
                }
                else if (terrain[x][y]==8) {//cas du block malus
                    addCase(new Malus_Niveau(this), x, y);
                }
                else if (terrain[x][y]==9) {//cas du second joueur
                    addCase(new Vide(this), x, y);
                    if(n == 0 || n == 1 ){
                        heros2 = new Heros(this, grilleEntites[x][y]);
                        second_heros_present = true;
                        heros2.setControlable_false();
                    }else
                    {
                        second_heros_present = false;
                    }
                    heros2.setId(1);

                }
                else if (terrain[x][y]==10) {//cas du block destructibles
                    addCase(new Vide(this), x, y);
                    bd[nbBD] = new Bloc_lourd(this,grilleEntites[x][y]);
                    nbBD++;
                }

            }
        }
        if(nbTP>0)  //on set manuellement les coord des TP (et non de la T_map)
        {
            if(nbTP==2){
                t[0].set_tp_a_tp(t[1].get_point_p_x(),t[1].get_point_p_y());
                t[1].set_tp_a_tp(t[0].get_point_p_x(),t[0].get_point_p_y());
            }
            if(nbTP==4){
                t[0].set_tp_a_tp(t[1].get_point_p_x(),t[1].get_point_p_y());
                t[1].set_tp_a_tp(t[0].get_point_p_x(),t[0].get_point_p_y());
                t[2].set_tp_a_tp(t[3].get_point_p_x(),t[3].get_point_p_y());
                t[3].set_tp_a_tp(t[2].get_point_p_x(),t[2].get_point_p_y());
            }
        }
    }

    // Permet d'initialiser les niveaux
    private void initialisationNiveau(int niveau_actuel) {

        outil=new Outil();
        if(getMode_2_joueurs())
        {
            outil.lire_un_fichier("././terrain/coop"+niveau_actuel+".txt");
        }else
        {
            outil.lire_un_fichier("././terrain/niveau"+niveau_actuel+".txt");
        }
        construire_terrain(0,outil);
    }

    private void inititaliser_tp(){     //seconde carte pour la tp_map

        outil=new Outil();
        if(getMode_2_joueurs())
        {
            outil.lire_un_fichier("././terrain/cooptp"+numero_niveau+".txt");
        }else
        {
            outil.lire_un_fichier("././terrain/tp"+numero_niveau+".txt");
        }
        construire_terrain(1,outil);
    }

    // Initialise la carte de retour apres la TP_map
    private void initialiser_retour_pap(int niveau_actuel){
        compteurCheckPoint=0;
        outil=new Outil();
        if(getMode_2_joueurs())
        {
            outil.lire_un_fichier("././terrain/coop"+niveau_actuel+".txt");
        }else
        {
            outil.lire_un_fichier("././terrain/niveau"+niveau_actuel+".txt");
        }
        construire_terrain(2, outil);
    }


    private void malus_map_retour(int niveau_actuel){
        outil=new Outil();
        if(getMode_2_joueurs())
        {
            outil.lire_un_fichier("././terrain/coop"+niveau_actuel+".txt");
        }else
        {
            outil.lire_un_fichier("././terrain/niveau"+niveau_actuel+".txt");
        }
        construire_terrain(3, outil);
    }



    //Pour ajouter une case
    private void addCase(Case e, int x, int y) {
        grilleEntites[x][y] = e;
        map.put(e, new Point(x, y));
    }

    //La condition de fin du jeu

    private void changement_carte(){inititaliser_tp();} //permet de basculer sur les cartes TP !terrain

    /** Si le déplacement de l'entité est autorisé (pas de mur ou autre entité), il est réalisé
     * Sinon, rien n'est fait.
     */
    public boolean deplacerEntite(Entite e, Direction d) {
        boolean retour = true;
        Point pCourant = map.get(e.getCase());
        Point pCible = calculerPointCible(pCourant, d);

        //Condition pour ne pas pousser plusieurs blocs entre eux
        if(e instanceof  Bloc || e instanceof  Bloc_lourd || e instanceof  Bloc_sans_objt){
            //On traitre le cas ou le Heros veut pousser plusieurs bloc à la fois
            Entite ecible = caseALaPosition(pCible).getEntite();
            if (ecible instanceof Entite || caseALaPosition(pCible) instanceof Mur) {
                return false;
            }
        }

        //Condition pour detecter lorsque le Hero marche sur une case malus
        if(e instanceof Heros){
            if(caseALaPosition(pCible) instanceof  Malus_Niveau){
                //On traite le cas lorsque nous passons sur une case malus, donc on regresse de niveau (si possible)
                passer_malus();
            }
        }

        //Condition pour pouvoir pousser les blocs lourds
        if(caseALaPosition(pCible).getEntite() instanceof Bloc_lourd) {
            if (i<3) {
                i++;
            }else{
                ((Bloc_lourd) caseALaPosition(pCible).getEntite()).setEst_pousse();
                ((Bloc_lourd) caseALaPosition(pCible).getEntite()).pousser(d);
            }
        }

        //Permet de traiter les differents cas (aller-retour) pour les TP_map
        if(caseALaPosition(pCible) instanceof TP_map){
            //On traitre la cas ou le Heros passe sur une TP_map, on gère le cas ou nous changeons de carte et nous revenons à l'originel
            if(!t_map.get_changez_carte()){
                A_travers_autre_carte = true;
                Tp_utiliser_nether = true;
                t_map.set_changez_carte();
                changement_carte();
            }else {
                A_travers_autre_carte=false;
                initialiser_retour_pap(numero_niveau);
            }
        }
        if (contenuDansGrille(pCible)) {
            Entite eCible = caseALaPosition(pCible).getEntite();
            if (eCible != null) {
                eCible.pousser(d);
            }

            // si la case est libérée
            if (caseALaPosition(pCible).peutEtreParcouru()) {
                e.getCase().quitterLaCase();

                //Permet de traiter le cas des TP (au sein de la meme carte)
                if(caseALaPosition(pCible) instanceof TP && ((e instanceof Heros)))
                {
                    Tp_utiliser = true;
                    //On traite le cas lorsque le Heros va sur une TP, nous changeons ces coordonnées
                    Point nouvelle_coord = new Point(((TP) caseALaPosition(pCible)).get_tp_a_tp());
                    caseALaPosition(nouvelle_coord).entrerSurLaCase(e);

                }else {
                    Tp_utiliser = false;
                    caseALaPosition(pCible).entrerSurLaCase(e);
                }


                //ce if permet de verifier si un bloc passe a un checkpoint, si c'est le cas il
                //met a true le parcourus du checkPoint
                if(e instanceof Bloc)
                {
                    //Condition pour mettre fin au niveau
                    if(caseALaPosition(pCible) instanceof CheckPoint)
                    {
                        ((CheckPoint) caseALaPosition(pCible)).setParcourus(true);
                        compteurCheckPoint++;
                    }
                    else if(caseALaPosition(pCourant) instanceof CheckPoint)
                    {
                        ((CheckPoint) caseALaPosition(pCourant)).setParcourus(false);
                        compteurCheckPoint--;
                    }
                }

            } else {
                retour = false;
            }
        } else {
            retour = false;
        }
        return retour;
    }


    private Point calculerPointCible(Point pCourant, Direction d) {
        Point pCible = null;

        switch(d) {
            case haut: pCible = new Point(pCourant.x, pCourant.y - 1); break;
            case bas : pCible = new Point(pCourant.x, pCourant.y + 1); break;
            case gauche : pCible = new Point(pCourant.x - 1, pCourant.y); break;
            case droite : pCible = new Point(pCourant.x + 1, pCourant.y); break;
        }
        return pCible;
    }



    /** Indique si p est contenu dans la grille
     */
    private boolean contenuDansGrille(Point p) {
        return p.x >= 0 && p.x < SIZE_X && p.y >= 0 && p.y < SIZE_Y;
    }

    private Case caseALaPosition(Point p) {
        Case retour = null;

        if (contenuDansGrille(p)) {
            retour = grilleEntites[p.x][p.y];
        }
        return retour;
    }

}