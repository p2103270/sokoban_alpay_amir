package modele;

import java.awt.*;

//Case qui permet de deplacer le personnage à travers une autre carte
public class TP_map extends Case{


    private Point p;
    public int id;
    private boolean parcourus=false;
    private boolean changez_carte=false;

    public TP_map(Jeu _jeu){super(_jeu);}

    public boolean peutEtreParcouru(){return true;}

    public boolean getParcourus(){return parcourus;}

    public int get_Id_tp(){return id;}
    public void set_id_tp(int x){
        id = x;
    }

    public boolean get_changez_carte(){return changez_carte;}

    public void set_changez_carte(){changez_carte = true;}

}
