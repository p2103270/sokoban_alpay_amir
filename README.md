# SOKOBAN_Alpay_Amir
KARANFIL Alpay
BENZIANE Amir 


LIFAPOO, Projet SOKOBAN

Bienvenue dans notre version du Sokoban, 
Le but du jeu est le suivant : vous devez pousser les blocs Jaunes sur les pièces d'or. 
Il faut que toutes les pièces présentes sur la carte soient occupées par les blocs d'or (jaune) pour pouvoir passer au niveau suivant. 

Chaque niveau possède son lots de particularités, à vous de les découvrir. 

Lancement du jeu : 
Pour lancer le Jeu, il suffit de run le projet (faire attention aux versions compatibles).
Pour profitez d'une meilleur expérience jouez avec le son, car le jeu en contient. 
Une fois le jeu lancé vous aurez l'écran d'accueil à vous de choisir si vous souhaitez jouez en solo ou en duo. 

Touches :

    Mode solo : 
        left arrow : se déplacer à gauche 
        right arrow : se déplacer à droite
        up arrow : se déplacer en haut 
        down arrow : se déplacer en bas
        r : relancer le niveau 
        echap : quitter
        espace : alterner de joueur (si second joueur présent)

    Mode duo : 
        left arrow : se déplacer à gauche, le personnage à droite
        right arrow : se déplacer à droite,  le personnage à droite
        up arrow : se déplacer en haut, le personnage à droite
        down arrow : se déplacer en bas, le personnage à droite
        z : se déplacer en haut, le personnage à gauche
        q : se déplacer à gauche,  le personnage à gauche
        s : se déplacer en bat, le personnage à gauche
        d :se déplacer à droite, le personnage à gauche
        r : relancer le niveau 
        echap : quitter
        espace : alterner de joueur (si second joueur présent) 


